variable "vsphere_user" {
}

variable "vsphere_password" {
  
}

variable "vsphere_server" {
  
}

variable "vsphere_host_IP" {
  
}

variable "vsphere_cluster" {
  
}

variable "vsphere_esxi_hosts" {

}

variable "vsphere_vswitch_name" {
  
}

variable "VLAN" {
  
}

variable "network_range" {
  
}

variable "dhcp_scope_name" {
  
}

variable "administrator_password" {
  
}

variable "vsphere_datastore" {
  
}

variable "prefix" {
  
}

variable "vsphere_vm_template_windows_non-core" {
  
}
