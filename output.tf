output "Folder_Name" {
  value = vsphere_folder.folder.path
}

output "Port_Group" {  
  value = "EPG${var.VLAN}"
}

output "Windows_DHCP_Server_Name" {
  value = vsphere_virtual_machine.vm_dhcp_server.name
}

output "Windows_DHCP_Server_IP_Address" {
  value = vsphere_virtual_machine.vm_dhcp_server.default_ip_address
}

output "Windows_DHCP_Server_DHCP_Range" {
  value = "${var.network_range}.${var.VLAN}.10 - ${var.network_range}.${var.VLAN}.200"
}
