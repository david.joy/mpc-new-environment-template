terraform {
  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = "2.0.2"
    }
  }
}


provider "vsphere" {
  # Configuration options
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = var.vsphere_server

  # If you have a self-signed cert
  allow_unverified_ssl = true
}


data "vsphere_datacenter" "dc" {
  name = "Datacenter"
}

data "vsphere_datastore" "datastore" {
  name          = var.vsphere_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_host" "host" {
  for_each = toset(var.vsphere_esxi_hosts)

  name          = each.value
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_host_port_group" "pg" {
  for_each = data.vsphere_host.host

  name           = "EPG${var.VLAN}"
  host_system_id = each.value.id
  # host_system_id      = data.vsphere_host.host.id
  vlan_id             = var.VLAN
  virtual_switch_name = var.vsphere_vswitch_name
}

resource "vsphere_folder" "folder" {
  path          = "${var.network_range}.${var.VLAN}.0_${var.prefix}"
  type          = "vm"
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_folder" "folder_template" {
  depends_on = [
    resource.vsphere_folder.folder
  ]
  path          = "${var.network_range}.${var.VLAN}.0_${var.prefix}/Templates"
  type          = "vm"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  name          = "EPG${var.VLAN}"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_compute_cluster" "compute_cluster" {
  name          = var.vsphere_cluster
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_resource_pool" "resource_pool" {
  name                    = "${var.network_range}.${var.VLAN}.0_${var.prefix}"
  parent_resource_pool_id = data.vsphere_compute_cluster.compute_cluster.resource_pool_id
}

data "vsphere_virtual_machine" "template_windows_non-core" {
  name          = var.vsphere_vm_template_windows_non-core
  datacenter_id = data.vsphere_datacenter.dc.id
}



locals {
  custom_command_dhcp_server = ["powershell.exe \"Install-WindowsFeature DHCP -IncludeManagementTools -IncludeAllSubFeature\"",
    "netsh dhcp add securitygroups",
    "powershell.exe \"Restart-Service dhcpserver\"",
    "powershell.exe \"Add-DhcpServerv4Scope -name '${var.dhcp_scope_name}' -StartRange ${var.network_range}.${var.VLAN}.10 -EndRange ${var.network_range}.${var.VLAN}.200 -SubnetMask 255.255.255.0 -State Active\"",
    "powershell.exe \"Set-DhcpServerv4OptionValue -OptionID 3 -Value ${var.network_range}.${var.VLAN}.1\"",
    "powershell.exe \"Set-DhcpServerv4OptionValue -DnsDomain ${var.dhcp_scope_name}.local -DnsServer 8.8.8.8\""
  ]
}


## Windows Core - DHCP Server ##

resource "vsphere_virtual_machine" "vm_dhcp_server" {
  name             = "${var.prefix}-DHCP"
  resource_pool_id = resource.vsphere_resource_pool.resource_pool.id
  datastore_id     = data.vsphere_datastore.datastore.id
  num_cpus         = data.vsphere_virtual_machine.template_windows_non-core.num_cpus
  memory           = data.vsphere_virtual_machine.template_windows_non-core.memory
  guest_id         = data.vsphere_virtual_machine.template_windows_non-core.guest_id
  folder           = resource.vsphere_folder.folder.path
  firmware         = "efi"

  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = data.vsphere_virtual_machine.template_windows_non-core.network_interfaces.0.adapter_type
  }

  disk {
    label            = "disk"
    size             = 60
    thin_provisioned = true
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template_windows_non-core.id

    customize {
      windows_options {
        computer_name         = "${var.prefix}-DHCP"
        domain_admin_user     = "Administrator"
        admin_password        = "${var.administrator_password}"
        auto_logon            = true
        auto_logon_count      = 1
        run_once_command_list = local.custom_command_dhcp_server
      }

      network_interface {
        ipv4_address = "${var.network_range}.${var.VLAN}.2"
        ipv4_netmask = 24
      }

      ipv4_gateway    = "${var.network_range}.${var.VLAN}.1"
      dns_server_list = ["${var.network_range}.${var.VLAN}.2", "8.8.8.8"]


    }
  }

}

